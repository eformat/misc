#!/usr/bin/env python
######################################################################
#
# Nagios plugin to system for expiring passwords
#
# Author Chris Callegari <ccallega@redhat.com>
#
######################################################################


import argparse
import os
import subprocess
from datetime import datetime, timedelta
from subprocess import check_output


class accountpasswordchecker():
    """Query password database for users and check the status of their
    passwords."""

    parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser(description='Query password database for \
    users and check the status of their passwords.')
    parser.add_argument(
        '-exp', '--expiration', type=int, default=5,
        help='Specify expiration lead in days (default: 5)')
    parser.add_argument(
        '-ha', '--zadmin', action='store_true', default=True,
        help='Include zadmin account (default: True)')
    parser.add_argument(
        '-r', '--root', action='store_true', default=False,
        help='Include root account (default: False)')
    parser.add_argument(
        '-d', '--daemon', action='store_true', default=False,
        help='Include daemon accounts (default: False)')
    parser.add_argument(
        '-s', '--svc', action='store_true', default=False,
        help='Include microservice accounts (default: False)')
    parser.add_argument(
        '-npe', '--nopassexp', action='store_true', default=False,
        help='Warn if password expiration set to never (default: False)')

    args = parser.parse_args()

    daemonusers = ['daemon', 'bin', 'sys', 'sync', 'games', 'man', 'lp',
                   'mail', 'news', 'uucp', 'proxy', 'www-data', 'backup',
                   'list', 'irc', 'gnats', 'nobody', 'libuuid', 'syslog',
                   'messagebus', 'ntp', 'sshd', 'vagrant', 'vboxadd',
                   'statd', 'postfix']

    svcusers = ['abcdef', 'nagios', 'munin', 'mysql', 'mongodb', 'rabbitmq',
                'redis', 'tomcat6', 'nginx', 'git', 'memcache', 'jboss7']

    def __init__(self):
        self.getent = self.userfind()
        self.users = self.userproc(self.getent)
        self.stdout(self.users)

    def datetime(self, expdate):
        try:
            if (datetime.now() - timedelta(days=self.args.expiration)) > \
                    datetime.strptime(expdate[0], '%b %d %Y'):
                return True
        except:
            pass

    def stdout(self, users):
        if ('2' in (a[0] for a in users.itervalues())):
            print('CRIT | Some account password(s) about to expire')
        elif ('1' in (a[0] for a in users.itervalues())) and
        self.args.nopassexp:
            print('WARN | Some accounts do not have expiration set')
        else:
            print('OK: all accounts ok |')

        for k, v in users.iteritems():
            print(k + v[1])

    def userfind(self):
        return(check_output(['/usr/bin/getent', 'passwd']).
               split('\n'))

    def userchage(self, user):
        return(check_output(['/usr/bin/chage', '-l', user]).
               replace('\t', '').
               replace(', ', ' ').
               split('\n'))

    def userprocc(self, user, users):
        expdate = (self.userchage(user[0])[1].split(': ')[1]).split(':')
        if 'never' not in expdate[0] and self.datetime(expdate):
            users[user[0]] = ['2',
                              ': passwd expire(d|s) on... %s' % (expdate[0])]
        elif 'never' in expdate and self.args.nopassexp:
            users[user[0]] = ['1',
                              ': no passwd expiration set']
        else:
            pass
        return(users)

    def userproc(self, getent):
        users = {}
        for user in getent:
            user = user.rstrip().split(':')
            if self.args.zadmin:
                if 'zadmin' in user[0]:
                    users.update(self.userprocc(user, users))
            if self.args.root:
                if 'root' in user[0]:
                    users.update(self.userprocc(user, users))
            if self.args.daemon:
                if any(x in user[0] for x in self.daemonusers):
                    users.update(self.userprocc(user, users))
            if self.args.svc:
                if any(x in user[0] for x in self.svcusers):
                    users.update(self.userprocc(user, users))
        return(users)


accountpasswordchecker()
