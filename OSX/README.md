# Fix pip
```
sudo rm -f /usr/bin/easy_install*
sudo rm -f /usr/local/bin/easy_install*

curl -O https://svn.apache.org/repos/asf/oodt/tools/oodtsite.publisher/trunk/distribute_setup.py
sudo python distribute_setup.py
sudo rm distribute_setup.py
```

# Fix six
```
sudo easy_install -U six
```

# Fix python-dateutil
```
sudo easy_install -U python-dateutil
```
