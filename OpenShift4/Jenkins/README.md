```
$ make
env var JENKINS_USER=<jenkins user> is req'd
env var JENKINS_TOKEN=<jenkins user token> is req'd

Make targets...
cr_job:	 Create a job in Jenkins
rm_job:	 Remove a job from Jenkins
arc_job:	 Archive a Jenkins job configuration

Additional target args...
FLAG=jobname ... Specify job name
```