#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Tool for retrieving next available ip in AWS subnet

@author:  "Chris Callegari" <ccallega@redhat.com>
"""

import boto.ec2
import boto.vpc
import netaddr
import operator


class privateipaddress_gen():

    def __init__(self, size):
        #DEBUG print(options)
        conn_ec2 = self.conn_ec2(options.region)
        subnets = self.enis(conn_ec2,options.region,options.vpc,options.subnet)
        conn_vpc = self.conn_vpc(options.region,options.vpc)
        subnets = self.enis2(conn_vpc,subnets)
        subnets = self.ip_process(subnets)
        self.output(subnets)

    def regions(self, args):
        # Add valid regions to this array when Red Hat allows those regions to
        # be used
        valid_r = [ 'us-east-1', 'us-west-1', 'us-west-2' ]
        if not options.region:
            regions = valid_r
        else:
            regions = [ ]
            for i in options.region:
                if i in valid_r:
                    regions.append(i)
        return regions

    def conn_ec2(self,r):
        #DEBUG print('conn')
        try:
            conn_ec2 = boto.ec2.connect_to_region(r)
        except Exception as ex:
            print("Error:  Could not connect to AWS API "
                "1. Check your network connection "
                "2. Check your boto credentials")
            print(ex)
            exit(1)
        return conn_ec2

    def conn_vpc(self,r,vpc):
        #DEBUG print('conn')
        try:
            conn_vpc = boto.vpc.connect_to_region(r)
        except Exception as ex:
            print("Error:  Could not connect to AWS API "
                "1. Check your network connection "
                "2. Check your boto credentials")
            print(ex)
            exit(1)
        return conn_vpc

    def enis(self,conn_ec2,region,vpc,subnet):
        #DEBUG print('check_enis')
        enis = conn_ec2.get_all_network_interfaces()
        subnets = {}
        #DEBUG print(enis)
        for eni in enis:
            if str(eni.vpc_id) in vpc:
                if eni.subnet_id not in subnets:
                    subnets[str(eni.subnet_id)] = {}
                if 'name' not in subnets[str(eni.subnet_id)]:
                    subnets[str(eni.subnet_id)]['name'] = 'name'
                if 'cidr_block' not in eni.subnet_id:
                    subnets[str(eni.subnet_id)]['cidr_block'] = ''
                if 'ips' not in subnets[str(eni.subnet_id)]:
                    subnets[str(eni.subnet_id)]['ips'] = []
                if str(eni.private_ip_address) not in subnets[str(eni.subnet_id)]['ips']:
                    subnets[str(eni.subnet_id)]['ips'].append(str(eni.private_ip_address))
        if 'subnet' in subnet:
            keys = []
            for key in subnets:
                if subnet not in key:
                    keys.append(key)
            map(subnets.pop, keys)
        return(subnets)

    def enis2(self,conn_vpc,subnets):
        #DEBUG print(enis2)
        for key in subnets:
            i=conn_vpc.get_all_subnets(subnet_ids=key)
            subnets[str(key)]['cidr_block'] = str(i[0].cidr_block)
        for key in subnets:
            i=conn_vpc.get_all_subnets(subnet_ids=key)
            subnets[str(key)]['cidr_block'] = str(i[0].cidr_block)
        return subnets

    def ip_process(self,subnets):
        #DEBUG print('ip_process')
        for key in subnets:
            net = netaddr.IPNetwork(subnets[key]['cidr_block'])
            ip_list = list(net)
            for ip in ip_list:
                if str(ip) != str(netaddr.IPNetwork(subnets[key]['cidr_block']).network):
                    if str(ip) not in str(subnets[key]['ips']):
                        subnets[str(key)]['next_addr'] = ip
                        break
        return subnets

    def output(self,subnets):
        #DEBUG print('output')
        print(subnets)
        print('BEGIN AWS/CF JSON BITS...')
        print('')
        for key, value in sorted(subnets.items(), key=lambda(k,v):(v,k)):
            print('SUBNET: %s %s' % (key, subnets[key]['cidr_block']))
            print('%s %s %s%s%s' % ('"PrivateIpAddress"', ':', '"', subnets[key]['next_addr'], '",'))
            print('')


if __name__ == "__main__":
    import optparse

    parser = optparse.OptionParser(
        "%prog [options]",
        description="Tool for generating AWS CloudFormation"
        "PrivateIpAddress Key")

    parser.add_option(
        '-d',
        dest = 'debug',
        help = "Enable debug mode")

    parser.add_option(
        '-r',
        dest = 'region',
        default = 'us-east-1',
        help = "Check AWS enis in region (default value = 'us-east-1')")

    parser.add_option(
        '-s',
        dest = 'subnet',
        default = 'subnet',
        help = "Check AWS enis in subnet id (default value = NONE)")

#    Not yet
#    parser.add_option(
#        '-t',
#        dest = 'tag',
#        default = '',
#        help = "Check AWS enis in subnet with name tag (default value = NONE)")

    parser.add_option(
        '-v',
        dest = 'vpc',
        default = 'vpc-987762fa',
        help = "Check AWS enis in vpc (default value = 'vpc-987762fa'")

    (options, args) = parser.parse_args()

    #DEBUG print(output)

    privateipaddress_gen(options)
