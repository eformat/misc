#!/usr/bin/env python

"""
Tool for creating ServiceNow incidents for AWS EC2 events
@author:  "Chris Callegari" <ccallega@redhat.com>
"""

import __main__
import argparse
import boto.ec2
import boto.iam
import json
import os
import os.path
import pycurl
import re
import smtplib
import sys
import cStringIO
import textwrap
import urllib2

from subprocess import call

class aws_event_processor:

    def __init__(self):
        self.args = self.ds_args()
        self.regions = self.regions(self.args)
        objects = self.instance_events(self.regions)
        self.track(objects)

    def ds_args(self):
        parser = argparse.ArgumentParser(
            prog = os.path.basename(__file__),
            formatter_class = argparse.RawDescriptionHelpFormatter,
            description = textwrap.dedent('''\
    Tool for creating ServiceNow incidents for AWS EC2 events'''))
        parser.add_argument(
            '-vpc',
            dest = 'vpc',
            action = 'store_const',
            help = "Check instances in current vpc",
            const = sum)
        parser.add_argument(
            '-region',
            dest = 'region',
            help = "Check instances in region[s] (csv entries allowed)",
            nargs = "?")
        parser.add_argument(
            '-debug',
            dest = 'debug',
            action = 'store_const',
            help = "Enable debug output (email address is req'd)",
            const = sum)
        args = parser.parse_args()
        return args

    def regions(self, args):
        # Add valid regions to this array when Red Hat allows those regions to
        # be used
        valid_r = [ 'us-east-1', 'us-west-1', 'us-west-2' ]
        if not self.args.region:
            regions = valid_r
        else:
            regions = [ ]
            r = self.args.region.split(',')
            for i in r:
                if i in valid_r:
                    regions.append(i)
        return regions

    def instance_events(self, regions):
        if self.args.vpc:
            buf = cStringIO.StringIO()
            c = pycurl.Curl()
            c.setopt(c.URL,
                'http://169.254.169.254/latest/dynamic/instance-identity/' \
                'document')
            c.setopt(c.WRITEFUNCTION, buf.write)
            c.perform()
            admin_instance_json = json.loads(buf.getvalue())
            buf.close()
            r = str(admin_instance_json['region'])
            regions = [ r ]
            admin_vpc_id = str(boto.ec2.connect_to_region(
                admin_instance_json['region']).get_all_instances(
                instance_ids = admin_instance_json['instanceId'])[
                0].instances[0].vpc_id)
            if self.args.debug:
                print 'DEBUG: instances limited to vpc id:', admin_vpc_id

        if self.args.debug:
            print 'DEBUG: checking regions ', regions
        for r in regions:
            objects = { }

            conn_ec2 = boto.ec2.connect_to_region(r)
            if not conn_ec2:
                sys.exit('Error: Conn_ec2 cannot connect to AWS region: ' + r)
            if not self.args.debug:
                stats = conn_ec2.get_all_instance_status(
                    filters = {"event.code" : "*"})
            else:
                stats = conn_ec2.get_all_instance_status()
            for stat in stats:
                conn_iam = boto.iam.connection.IAMConnection()
                if not conn_iam:
                    sys.exit('Error: Conn_iam cannot connect to AWS ' \
                             'region: ' + r)
                acct_alias = conn_iam.get_account_alias()
                acct_alias = str(acct_alias.account_aliases[0])

                info = conn_ec2.get_all_instances(instance_ids = stat.id)

                try:
                    name = str(info[0].instances[0].tags['Name'])
                except KeyError, e:
                    name = 'Unknown'

                try:
                    vpc_id = str(info[0].instances[0].vpc_id)
                except KeyError, e:
                    vpc_id = 'Unknown'

                if stat.events and not self.args.debug:
                    for event in stat.events:
                        obj = { 'account':acct_alias,
                                'region':r,
                                'vpc_id':vpc_id,
                                'event':event.code,
                                'not_before':event.not_before,
                                'action':'none',
                                'name':name }
                else:
                    obj = { 'account':acct_alias,
                            'region':r,
                            'vpc_id':vpc_id,
                            'event':'blah_event_code',
                            'not_before':'blah_not_before',
                            'action':'none',
                            'name':name }

                if (not self.args.vpc) or (vpc_id in admin_vpc_id):
                    id = str(stat.id)
                    objects[id] = obj
                    if self.args.debug:
                        print 'DEBUG: appended json object:', stat.id, \
                                                              objects[stat.id]

        return objects

    def track(self, objects):
        if self.args.debug:
            print 'DEBUG:', objects
        if not os.path.isfile('/var/tmp/aws_ec2_events.json'):
            open('/var/tmp/aws_ec2_events.json', 'a')
        try:
            with open('/var/tmp/aws_ec2_events.json', 'r') as f:
                data = json.load(f)
        except ValueError as e:
            print "Error: File does not exist, empty or invalid json format"
            print "Error: Dumping json objects to new data file"
            f = open('/var/tmp/aws_ec2_events.json', 'w')
            json.dump(objects,
                      f,
                      sort_keys = True,
                      indent = 4,
                      ensure_ascii = False)
            with open('/var/tmp/aws_ec2_events.json', 'r') as f:
                data = json.load(f)
        for instance in objects:
            try:
                if data[instance]['action'] == 'none':
                    raise ValueError('Notice: Action has not yet been taken ' \
                                     'on:', data[instance])
                else:
                    objects[instance]['action'] = 'yes'
            except KeyError as e:
                print 'Notice: Taking action on:', instance
                print('Notice: Sending email to create ServiceNow Incident:'),
                if self.cr_snow_tkt(instance, objects[instance]):
                    objects[instance]['action'] = 'yes'
            except TypeError as e:
                print 'Notice: Taking action on:', instance
                print('Notice: Sending email to create ServiceNow Incident:'),
                if self.cr_snow_tkt(instance, objects[instance]):
                    objects[instance]['action'] = 'yes'
            except ValueError as e:
                print "Notice: Taking action on:", instance
                print('Notice: Sending email to create ServiceNow Incident:'),
                if self.cr_snow_tkt(instance, objects[instance]):
                    objects[instance]['action'] = 'yes'
        f = open('/var/tmp/aws_ec2_events.json', 'w')
        json.dump(objects,
                  f,
                  sort_keys = True,
                  indent = 4,
                  ensure_ascii = False)

    def cr_snow_tkt(self, instance, json):
        sender = 'it-syseng@redhat.com'
        if not self.args.debug:
            to    = 'Service Desk <redhat@service-now.com>'
            email = 'redhat@service-now.com'
        else:
            id = os.popen("logname").read().rstrip()
            email = "{0}@redhat.com".format(id)
            geckoss = os.popen("getent passwd").readlines()
            for geckos in geckoss:
                if id in geckos:
                    geckos = geckos.rstrip().split(":")
                    to = geckos[4] + ' <' + email + '>'
                    geckoss = None
                    geckos = None
                    break
            print '\nDEBUG: sending email to:', to
        message = ("From: IT-SysEng <it-syseng@redhat.com>\n"
        "To: {0}\n"
        "Subject: [FWD TO Platform Operations] AWS EC2 Event notice\n"
        "\n"
        "Account:     {1}\n"
        "Region:      {2}\n"
        "Vpc_ID:      {3}\n"
        "Event:       {4}\n"
        "Not Before:  {5} UTC\n"
        "Instance_ID: {6}\n"
        "Name:        {7}\n"
        "\n"
        "See the following link for more information regarding maintenance " \
        "operations conducted by AWS\n"
        "http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/monitoring-" \
        "instances-status-check_sched.html\n"
        ).format(to,
                 str(json['account']),
                 str(json['region']),
                 str(json['vpc_id']),
                 str(json['event']),
                 str(json['not_before']),
                 instance,
                 str(json['name']))

        if self.args.debug:
            print('DEBUG: locals():'), locals()
            print 'Notice: Sending email to create ServiceNow Incident:',
        try:
            smtpObj = smtplib.SMTP('smtp.redhat.com')
            smtpObj.sendmail(sender, email, message)
            print 'Success'
            return True
        except smtplib.SMTPException as ex:
            print 'Failed'
            print("Error: Unable to send email:\n{0}\n{1}".
                format(ex, locals()))
            return False


aws_event_processor()
