scripts
=======
video_file_proc.py

usage: video_file_proc.py [dir] [nzb] [name] [id] [cat] [group] [status]
                          [-h] [-t {m4v,mkv,avi,wmv,dvd}] [-v]
            

Process incoming video files, trancode, insert in to iTunes

Req'd configuration:
  
  1. Place this script in a folder somewhere under $HOME
  
  2. Install Sabnzb+
  
  3. Configure Sabnzb+

    Folders: 

      Ensure that "Post-Processing Scripts Folder" is pointing to the

      folder where this script exists

    Categories:
      
      Categories allowed are {tv,movie}
      
      Processing should be set to +Delete
      
      Script should be set to this_script.py
      
      Groups / Indexer tags set to sabnzb
  
  5. Configure upstream library services {SickBeard,CouchPotato,etc}
  
  6. HandBrakeCli required
      brew install HandBrakeCli
  
  7. AtomicParsley required
      brew install AtomicParsley
  
  8. Python modules req'd
 
     imdb, tvdb_api
  
  9. Let the magic begin!


Positional arguments:

  1. dir    :  The final directory of the job (full path)
  
  2. nzb    :  The original name of the NZB file
  
  3. name   :  Clean version of the job name (no path info and ".nzb"
               removed)
  
  4. id     :  Indexer's report number (if supported)
  
  5. cat    :  User-defined category
  
  6. group  :  Group that the NZB was posted in e.g. alt.binaries.x
  
  7. status :  Status of post processing. 0 = OK, 1 = failed
               verification, 2 = failed unpack, 3 = unused, -1 =
               failed download

Optional arguments:
  
  1. -h, --help : show this help message and exit
  
  2. -{t|type} {m4v,mkv,avi,wmv,dvd} : Select movie file type
  
  3. -v, --verbosity :     Increase output verbosity

<br><br>

check_iscsi.pl

Usage: $0 [--offload (y|yes|n|no)] [--mtu (1500|9000)] [--vlan (1-4049) [--sessions [0-9]+] [ -? | -h | --help ]

Available options: --offload hardware iSCSI offload, yes|no query "iscsiadm -m iface -P1" and parse

--mtu mtu size for iSCSI interfaces, 1500|9000 if offload=n, query "ip addr" and parse mtu if offload=y, query "iscsiadm -m iface -I $I | grep iface.mtu"

--vlan vlan id, 1-4094 (IEEE 802.1Q standard) if offload=n, query "iscsiadm -m host" -> obtain IP of initiator "ip addr | grep IP" -> obtain interface name "cat /sys/class/net/eth#/mtu" -> obtain mtu value if offload=y, "iscsiadm -m iface -I $I -o show | grep vlan_id"

--sessions total number active iSCSI sessions, (0-9)+ query "iscsiadm -m session -r SID -P3" and parse

NOTES: This plugin only issues state OK|WARN

ONLY works for the following interfaces... Standard Linux network interfaces Broadcom Corporation NetXtreme II 10 Gigabit Ethernet Chelsio Communications Inc 10GbE Port Adapters Send me lspci, "iscsiadm -m iface -P1", "iscsiadm -m host"

VLAN is experimental. The code looks like it should work but we don't vlan tag storage interfaces so I have no a test bed.
